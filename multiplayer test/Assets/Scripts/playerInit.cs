﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[BoltGlobalBehaviour("game")]
public class playerInit : Bolt.GlobalEventListener
{
    public override void SceneLoadLocalDone(string scene)
    {
        BoltConsole.Write("Spawn Player on map " + scene, Color.yellow);
        playerNetworking.Spawn();
    }
}