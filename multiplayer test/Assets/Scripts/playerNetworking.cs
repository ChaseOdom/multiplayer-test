﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class playerNetworking : Bolt.EntityBehaviour<ICubeState>
{
    public GameObject prefab;

    public override void Attached()
    {
        BoltConsole.Write("Attached normal player");
        state.SetTransforms(state.CubeTransform, transform);

        if (entity.IsOwner)
        {
            /*
            state.AddCallback("Color", () =>
            {
                GetComponent<MeshRenderer>().material.color = Color.blue;
            });*/
            state.CubeColor = new Color(Random.value, Random.value, Random.value);
        }
        state.AddCallback("CubeColor", ColorChanger);
    }

    void ColorChanger()
    {
        GetComponent<Renderer>().material.color = state.CubeColor;
    }

    public override void Initialized()
    {
        BoltConsole.Write("Initialized normal player");
    }

    public override void ControlGained()
    {
        BoltConsole.Write("ControlGained normal player", Color.blue);
    }

    public override void SimulateOwner()
    {
        var speed = 4f;
        var movement = Vector3.zero;

        if (Input.GetKey(KeyCode.W)) { movement.z += 1; }
        if (Input.GetKey(KeyCode.S)) { movement.z -= 1; }
        if (Input.GetKey(KeyCode.A)) { movement.x -= 1; }
        if (Input.GetKey(KeyCode.D)) { movement.x += 1; }

        if (Input.GetKey(KeyCode.F)) { movement.x += 1; }

        if (movement != Vector3.zero)
        {
            transform.position = transform.position + (movement.normalized * speed * BoltNetwork.FrameDeltaTime);
        }
    }

    private void Setup()
    {
        BoltConsole.Write("Setup player");

        if (entity.IsOwner)
        {

        }
    }


    public static void Spawn()
    {
        var pos = new Vector3(Random.Range(-1, 1), 0.6f, Random.Range(-1, 1));
        BoltEntity playerEntity = BoltNetwork.Instantiate(BoltPrefabs.NotACube, pos, Quaternion.identity);
        playerEntity.TakeControl();

        playerNetworking playerController = playerEntity.GetComponent<playerNetworking>();

        playerController.Setup();
    }
}
