﻿using Bolt.Matchmaking;
using Bolt.Samples.Photon.Simple;
using System;
using System.Collections;
using System.Collections.Generic;
using UdpKit;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Lobby : Bolt.GlobalEventListener
{
    public Canvas lobbyScreen;
    private int clientCount;
    public int maxClients = 4;

    // Start is called before the first frame update
    void Start()
    {
        clientCount = 0;
    }

    public void OnHost()
    {
        lobbyScreen.enabled = false;
        BoltLauncher.StartServer();
        //BoltLauncher.StartClient();
    }

    public void OnJoin()
    {
        lobbyScreen.enabled = false;
        BoltLauncher.StartClient();
    }

    private void JoinEventHandler(UdpSession session)
    {
        if (BoltNetwork.IsClient)
        {
            BoltNetwork.Connect(session);
        }
    }

    private void ShutdownEventHandler()
    {
        BoltLauncher.Shutdown();
    }


    //Callbacks
    public override void BoltStartBegin()
    {
        BoltNetwork.RegisterTokenClass<RoomProtocolToken>();
        BoltNetwork.RegisterTokenClass<ServerAcceptToken>();
        BoltNetwork.RegisterTokenClass<ServerConnectToken>();
    }

    public override void BoltStartDone()
    {


        if (BoltNetwork.IsServer)
        {
            var token = new RoomProtocolToken()
            {
                ArbitraryData = "My DATA",
            };

            BoltLog.Info("Starting Server");
            // Start Photon Room
            BoltMatchmaking.CreateSession(
                sessionID: "match1",
                token: token
            );
        }
        else if (BoltNetwork.IsClient)
        {
            BoltLog.Info("Starting Client");
            BoltMatchmaking.JoinRandomSession();
        }
    }

    public override void SessionCreated(UdpSession session)
    {

        // Build Server Entity
        var entity = BoltNetwork.Instantiate(BoltPrefabs.NotACube);
        entity.TakeControl();
    }

    public override void EntityAttached(BoltEntity entity)
    {
        
    }

    public override void Connected(BoltConnection connection)
    {
        if (BoltNetwork.IsClient)
        {
            clientCount++;
            BoltConsole.Write(string.Format("Connected Client: {0}", connection), Color.blue);

            playerNetworking.Spawn();
        }
        else if (BoltNetwork.IsServer)
        {
            BoltConsole.Write(string.Format("Connected Server: {0}", connection), Color.blue);


            /*
            var entity = BoltNetwork.Instantiate(BoltPrefabs.PlayerInfo);
            entity.AssignControl(connection);*/
        }
        //BoltConsole.Write(string.Format("Connected Client: {0}", connection), Color.blue);
    }



    public override void Disconnected(BoltConnection connection)
    {
        foreach (var entity in BoltNetwork.Entities)
        {
            //if (entity.StateIs<ILobbyPlayerInfoState>() == false || entity.IsController(connection) == false) continue;
        }
    }
}
